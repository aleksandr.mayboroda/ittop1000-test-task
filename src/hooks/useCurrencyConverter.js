import {useState} from 'react'

const useCurrencyConverter = () => {
  const [currencies,setCurrencies] = useState([])
  const [errors, setErrors] = useState({})

  //calculate by left input and both selects
  const calculateByLeft = (currentData) => {
    setErrors({})
    const rates = returnRateRatio(currentData)
      
      const hasErrors = validate('left', currentData)
      if (
        hasErrors
      ) {
        setErrors(hasErrors)
      } else {
        currentData.rightInput = (
          currentData.leftInput *
          (rates.left / rates.right)
        ).toFixed(2)
      }
    
    return currentData
  }

  //calculate by right input
  const calculateByRight = (currentData) => {
    setErrors({})
    const rates = returnRateRatio(currentData)
    const hasErrors = validate('right', currentData)
    if (
      hasErrors
    ) {
      setErrors(hasErrors)
    } else {
      currentData.leftInput = (
        currentData.rightInput *
        (rates.right / rates.left )
      ).toFixed(2)
    }
    return currentData
  }

  const validate = (side = 'left', currentData) => {
    const errors = {}
    if (side === 'left') {
      if (!currentData.leftInput) {
        errors.leftInput = 'Enter number in left input!'
      }
      if (!currentData.leftSelect) {
        errors.leftSelect = 'Select currency in left select!'
      }
      if (!currentData.rightSelect) {
        errors.rightSelect = 'Select currency in right select!'
      }
    } else {
      if (!currentData.rightInput) {
        errors.rightInput = 'Enter number in right input!'
      }
      if (!currentData.leftSelect) {
        errors.leftSelect = 'Select currency in left select!'
      }
      if (!currentData.rightSelect) {
        errors.rightSelect = 'Select currency in right select!'
      }
    }
    return Object.keys(errors).length > 0 ? errors : null
  }

  const returnCurrencyRate = (currency) => {
    const currencyObj = currencies.find(
      (curr) => curr.cc.toLowerCase() === currency.label
    )
    return currencyObj?.rate || 0
  }

  const returnRateRatio = currentData => {
    const rates = { left: 1, right: 1 }
    if (currentData.leftSelect.value === 'uah') {
      rates.left = 1
    } else {
      rates.left = returnCurrencyRate(currentData.leftSelect) || 1
    }

    if (currentData.rightSelect.value === 'uah') {
      rates.right = 1
    } else {
      rates.right = returnCurrencyRate(currentData.rightSelect) || 1
    }
    return rates
  }
  
  return {setCurrencies, setErrors, errors, calculateByLeft, calculateByRight}
}

export default useCurrencyConverter