
import './App.module.scss';

import Main from './pages/Main'
import Header from './components/Header'

const App = () => {
  return (
    <div className="container-vertical container">
      <Header />
      <Main />
    </div>
  );
}

export default App;
