import {combineReducers} from "redux";
import currencies from './currencies';


const reducers = combineReducers({
  currencies,
})

export default reducers