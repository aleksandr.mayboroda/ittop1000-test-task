import actions from './actions'
import { getCurrencies } from '../../api/currency'

const defaultCurrencies = ['usd', 'eur']

const fetchCurrencies = (date) => async (dispatch, getState) => {
  const date = getState().currencies.filters.date

  try {
    dispatch(actions.setIsLoading(true))
    const result = await getCurrencies(date)
    let data = await result.data
    if (data) {
      data = [
        ...data.filter((currency) =>
          defaultCurrencies.includes(currency.cc.toLowerCase())
        ),
      ]
    }

    dispatch(actions.setData(data))
    dispatch(actions.setIsLoading(false))
  } catch (err) {
    dispatch(actions.setError('Shit happens! Reload page plz'))
    dispatch(actions.setIsLoading(false))
  }
}

const def = {
  fetchCurrencies,
}

export default def
