import types from './types'
import moment from 'moment'

const initialState = {
  data: [],
  isLoading: false,
  error: null,
  filters: {
    date: moment().format(),
  }
}

const reducer = (state = {...initialState}, action) => {
  switch(action.type)
  {
    case types.SET_CURRENCIES: {
      return {...state, data: action.payload, isLoading: false, error: null}
    }
    case types.SET_IS_LOADING: {
      return {...state, isLoading: action.payload}
    }
    case types.SET_ERROR: {
      return {...state, error: action.payload}
    }
    default: 
    return state
  }
}

export default reducer