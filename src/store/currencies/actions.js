import types from './types'

const setData = (value) => ({
  type: types.SET_CURRENCIES,
  payload: value,
})

const setIsLoading = (value) => ({
  type: types.SET_IS_LOADING,
  payload: value,
})

const setError = (value) => ({
  type: types.SET_ERROR,
  payload: value,
})

const def = {
  setData, setIsLoading, setError,
}

export default def