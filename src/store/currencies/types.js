const SET_CURRENCIES = 'ittop1000/currencies/SET_CURRENCIES'
const SET_IS_LOADING = 'ittop1000/currencies/SET_IS_LOADING'
const SET_ERROR = 'ittop1000/currencies/SET_ERROR'

const def = {
  SET_CURRENCIES,
  SET_IS_LOADING,
  SET_ERROR,
}

export default def