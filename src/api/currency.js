import axios from 'axios'
import moment from 'moment'

const BASE_URL = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange'

export const getCurrencies = (date) => axios.get(`${BASE_URL}`,{params:{json: true, date: moment(date).format('YYYYMMDD')}})