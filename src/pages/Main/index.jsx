import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { currenciesOperations } from '../../store/currencies'

import styles from './main.module.scss'

import Form from '../../components/Form'

const Main = () => {
  const dispatch = useDispatch()

  const loadCurrencies = async () => {
    dispatch(currenciesOperations.fetchCurrencies())
  }

  useEffect(() => {
    loadCurrencies()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <div className={styles.main}>
        <h2 className={styles.main__title}>currency converter</h2>
        <Form />
      </div>
    </>
  )
}

export default Main
