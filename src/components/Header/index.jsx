import React from 'react'

import styles from './header.module.scss'

import CurrencyList from '../CurrencyList'

const Header = () => {
  return (
    <div className={styles.header}>
      <div className="container-inner">
        <CurrencyList />
      </div>
    </div>
  )
}

export default Header
