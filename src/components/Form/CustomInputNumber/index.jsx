import React from 'react'
import NumberFormat from 'react-number-format'

const CustomInputNumber = ({ name, value, errors, onValueChange, className = '', placeHolder = '', showError = true }) => {
  return (
    <>
      <NumberFormat
        className={className}
        thousandSeparator={true}
        allowNegative={false}
        name={name}
        value={value}
        onValueChange={onValueChange}
        placeHolder={placeHolder}
      />
      {showError && (name in errors) && (<p className="form__error">{errors[name]}</p>)}
    </>
  )
}

export default CustomInputNumber
