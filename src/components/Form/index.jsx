import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { currenciesSelectors } from '../../store/currencies'

import CustomInputNumber from './CustomInputNumber'
import CustomInputSelect from './CustomInputSelect'

import useCurrencyConverter from '../../hooks/useCurrencyConverter'

import SwitchBtn from './SwitchBtn'

import './form.scss'

const selectStyles = {
  control: (provided, _) => ({
    ...provided,
    borderRadius: '7px',
    fontSize: '16px',
  }),
}

const initialFormData = {
  leftInput: '',
  leftSelect: {},
  rightInput: '',
  rightSelect: {},
}

const Form = () => {
  const { data: currencies } = useSelector(currenciesSelectors.getData())
  const [options, setOptions] = useState([])

  const [formData, setFormData] = useState({ ...initialFormData })

  const { setCurrencies, errors, calculateByLeft, calculateByRight } =
    useCurrencyConverter()

  const myChange = (name, value) => {
    setFormData((prev) => {
      const currentData = { ...prev, [name]: value }
      const result =
        name === 'rightInput'
          ? calculateByRight(currentData)
          : calculateByLeft(currentData)
      return { ...result }
    })
  }

  const onInputChange = (values, sourceInfo) => {
    if (sourceInfo.event) {
      myChange(sourceInfo.event.target.name, values.floatValue)
    }
  }

  const onSelectChange = (value, { name }) => myChange(name, value)

  const switchCurrencies = async () => {
    console.log('switch')
    const [left, right] = [formData.rightSelect, formData.leftSelect]
    setFormData({ ...formData, leftSelect: left, rightSelect: right })
    setFormData((prev) => {
      const currentData = { ...prev }
      const result = calculateByLeft(currentData)
      return { ...result }
    })
  }

  useEffect(() => {
    if (currencies.length > 0) {
      const defaultUah = { label: 'uah', value: 'uah' }
      const arr = [
        defaultUah,
        ...currencies.map((curr) => ({
          label: curr.cc.toLowerCase(),
          value: curr.cc.toLowerCase(),
        })),
      ]
      setOptions(arr)
      setFormData({ ...formData, leftSelect: arr[0], rightSelect: arr[1] })

      //test
      setCurrencies(currencies)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currencies])

  return (
    <div className='form-container'>
      <form className="form">
        <div>
          <div className="form__form-element">
            <CustomInputNumber
              name="leftInput"
              value={formData.leftInput}
              onValueChange={onInputChange}
              errors={errors}
              showError={false}
              className="form__field"
              placeHolder='type the sum here'
            />
          </div>

          {options.length > 0 && (
            <div className="form__form-element">
              <CustomInputSelect
                name="leftSelect"
                options={options}
                value={formData.leftSelect}
                errors={errors}
                showError={false}
                onChange={onSelectChange}
                isOptionDisabled={(option) => option === formData.rightSelect}
                styles={selectStyles}
              />
            </div>
          )}
        </div>

        <SwitchBtn onClick={switchCurrencies} />
        <div>
          <div className="form__form-element">
            <CustomInputNumber
              name="rightInput"
              value={formData.rightInput}
              onValueChange={onInputChange}
              errors={errors}
              showError={false}
              className="form__field"
              placeHolder='...or here'
            />
          </div>
          {options.length > 0 && (
            <div className="form__form-element">
              <CustomInputSelect
                name="rightSelect"
                options={options}
                value={formData.rightSelect}
                errors={errors}
                showError={false}
                onChange={onSelectChange}
                isOptionDisabled={(option) => option === formData.leftSelect}
                styles={selectStyles}
              />
            </div>
          )}
        </div>
      </form>
      {Object.keys(errors).length > 0 &&
        Object.values(errors).map((err, index) => (
          <p key={index} className="form__error">
            {err}
          </p>
        ))}
    </div>
  )
}

export default Form
