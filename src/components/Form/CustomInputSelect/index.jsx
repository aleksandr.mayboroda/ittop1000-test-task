import React from 'react'
import Select from 'react-select'

const CustomInputSelect = ({
  options,
  errors,
  name,
  value,
  onChange,
  isOptionDisabled,
  styles = {},
  showError = true,
}) => {
  return (
    <>
      <Select
        options={options}
        value={value}
        name={name}
        onChange={onChange}
        isOptionDisabled={isOptionDisabled}
        styles={styles}
      />
      {showError && name in errors && (
        <p className="form__error">{errors[name]}</p>
      )}
    </>
  )
}

export default CustomInputSelect
