import React from 'react'
import { FaArrowsAltH } from 'react-icons/fa'

import styles from './switch.module.scss'

const SwitchBtn = ({ onClick }) => {
  return (
    <div className={styles.switch__block}>
      <button className={styles.switch__btn} onClick={onClick} type="button">
        <FaArrowsAltH size={'25px'} />
      </button>
    </div>
  )
}

export default SwitchBtn
