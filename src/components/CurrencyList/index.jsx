import React from 'react'

import { useSelector } from 'react-redux'
import { currenciesSelectors } from '../../store/currencies'

import styles from './currencyList.module.scss'

const CurrencyList = () => {
  const { data } = useSelector(currenciesSelectors.getData())

  if (data.length > 0) {
    return (
      <>
        <ul className={styles.currency__list}>
          {data.map((currency) => (
            <li key={currency.r030} className={styles.currency__item}>
              {currency.txt} ({currency.cc.toLowerCase()}):
              <span className={styles.currency__value}>
                {currency.rate.toFixed(2)}
              </span>
            </li>
          ))}
        </ul>
      </>
    )
  }

  return <p className={styles.currency__empty}>no data is loaded</p>
}

export default CurrencyList
